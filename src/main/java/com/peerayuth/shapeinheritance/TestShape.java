/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.shapeinheritance;

/**
 *
 * @author Ow
 */
public class TestShape {
    public static void main(String[] args) {
        Circle circle = new Circle(3);
        circle.calArea();
        circle.print();
        
        Circle circle1 = new Circle(4);
        circle1.calArea();
        circle1.print();
        
        Triangle triangle = new Triangle(3,4);
        triangle.calArea();
        triangle.print();
        
        Rectangle rectangle = new Rectangle(4,3);
        rectangle.calArea();
        rectangle.print();
        
        Square square = new Square(2);
        square.calArea();
        square.print();
        
        Shape[] shapes = {circle, triangle, rectangle, square};
        for (int i = 0 ; i < shapes.length ; i ++) {
            shapes[i].print();
        }
    }
}
