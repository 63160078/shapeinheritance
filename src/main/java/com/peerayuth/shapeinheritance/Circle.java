/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.shapeinheritance;

/**
 *
 * @author Ow
 */
public class Circle extends Shape{
    private double r;
    static final double pi = 22.0/7;
    
    public Circle(double r) {
        System.out.println("Circle Created");
        this.r = r;
    }
    
    @Override
    public double calArea() {
        return pi * r * r;
    }
    
    @Override
    public void print() {
        super.print();
        System.out.println(calArea());
    }
}
