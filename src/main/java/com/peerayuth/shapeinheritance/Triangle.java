/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.shapeinheritance;

/**
 *
 * @author Ow
 */
public class Triangle extends Shape{
    private double h;
    private double w;
    
    static final double x = 0.5;
    
    public Triangle(double h,double w) {
        System.out.println("Triangle Created");
        this.h = h;
        this.w = w;
    }
    
    @Override
    public double calArea() {
        return x*h*w;
    }
    
    
    @Override
    public void print() {
        super.print();
        System.out.println(calArea());
    }
    
}
