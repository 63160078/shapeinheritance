/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.shapeinheritance;

/**
 *
 * @author Ow
 */
public class Rectangle extends Shape{
    private double l;
    private double w;
    
    public Rectangle(double l,double w) {
        System.out.println("Rectangle Created");
        this.l = l;
        this.w = w;
    }
    
    @Override
    public double calArea() {
        return l*w;
    }
    
    
    @Override
    public void print() {
        super.print();
        System.out.println(calArea());
    }
}
